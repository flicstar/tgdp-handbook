# The Good Docs Project Handbook

This repo contains documentation for The Good Docs Project.

The handbook uses the static site generator [HUGO](https://gohugo.io/) along with the [Doks theme](https://getdoks.org/).
