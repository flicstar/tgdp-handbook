---
title: "Content strategy"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "working groups"
weight: 020
toc: true
---

## Scope of work

The Content Strategy working group is responsible for a variety of focus areas in The Good Docs Project including:

* Content Strategy governance
* Website
* Knowledge base
* Social media
* Blog
* Project advocacy
* UX

Some focus areas are discrete projects with a start and end date, others are on-going activities. UX is a special focus area defined as a service. See [Focus Areas](#focus-areas) for more detail.


### Content Strategy in The Good Docs Project

The Content Strategy working group relies on the [Core Strategy](https://gitlab.com/tgdp/governance/-/blob/main/Core_Strategy.md) of The Good Doc’s Project so that:

**As a project**, we can:



* Have a touchstone or “North Star” to guide decisions about content produced by the project, as well as communications about the project.
* Reinforce the foundation that is already underlying our current initiatives.
* Adapt easily to support our audience. As we grow, our audience will grow and their needs might change.

**As a community**, we can:



* Hold a shared idea of what we’re about—so that we’re on the same page.
* Present a unified idea, and use consistent language, when talking about our products and the identity of our project to others.

**As content producers**, we can:



* Address content challenges, prioritize them accordingly and form solutions.
* Tie each piece of content that we create back to a community or project goal.


### Our audience

Our primary audience includes:



* Users of our products
* Members of our community
* People seeking to learn more about the technical writing domain.


### Our content

The Good Docs Project is primarily a content project. Our product is content in the form of templates, project documentation and educational content. This content is served via our website and GitLab. We have other incidental channels like social media and conferences.

Each piece of content must be tied to at least one community or project goal, that is in keeping with the project’s core strategy.


## Roles and responsibilities

The working group has a lead (or two co-leads) who are responsible for: 



* Managing and hosting meetings.
* Facilitating workshops (or engaging others to do so)
* Following up on action items and outcomes from meetings.
* Grooming the backlog.
* Release planning. Helping the team plan for, and commit to, work for each release.
* Social media posting.
* Keeping focus area leads on-track and making sure they’re not blocked.

Co-leads for 2023: Carrie Crowe and Felicity Brand.


## How we communicate


### Slack

The team uses the [#content-strategy](https://thegooddocs.slack.com/archives/C023G344YAJ) Slack channel.


### Meetings

The team meets weekly on Thursday 1915 EST / Friday 1015 AEDT, and keeps [meeting minutes](https://docs.google.com/document/d/1x6SExdCncsbRb3XMUYyshXcPo5OIFxT81fvComNJjkc/edit#).

Meeting cadence for 2023: Every second meeting is devoted to a workshop. This allows us working time to make progress on open issues. In quarter one, the workshops are dedicated to the Website project so that we can meet website commitments for the Capilano release.

In the off-weeks, the meetings hold space for updates from each focus area, and allow time to discuss any open issues earmarked for input from the content strategy team.


## How we work


### How work is recorded

All tasks for the Content Strategy working group are captured as issues in GitLab.

Content Strategy issues are raised in the repo most appropriate for the work. This might be the focus area, or where the completed work will be implemented - for example, website tasks are raised as issues in the [Website repo](https://gitlab.com/tgdp/website). The majority of Content Strategy issues are held in the [Governance and Content and Community repo](https://gitlab.com/tgdp/governance). 


### How work is tracked

We use GitLab to categorize and track work.



* All content strategy tasks use the “wg:Content Strategy” label.
* Each focus area has a sub-label to further categorize it. For example, “cs-governance”, “cs-project advocacy”.
* Labels are also used to track the status of a task: 
    * “Workflow-backlog”
    * “Workflow-in flight”
    * “Workflow-done”.
* The issues are visually represented on the [Content Strategy kanban board](https://gitlab.com/tgdp/governance/-/boards/4842980?label_name[]=wg%3A%3AContent%20strategy).


### How to raise work for the team

Anyone can raise an issue for the content strategy working team. Work might arise out of another TGDP meeting, from a conversation in Slack, or when it’s recognised as a task by a Content Strategy team member.

The working group leads ensure that issues are raised in the appropriate repo, that they’re labeled accordingly, and that they come up for discussion in a team meeting. 


## Focus areas


<table>
  <tr>
   <td><strong>Focus area</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
   <td><strong>Lead</strong>
   </td>
   <td><strong>Communication</strong>
   </td>
  </tr>
  <tr>
   <td>Governance
   </td>
   <td>Admin
   </td>
   <td>This covers the administrative tasks of running the Working Group. This work is on-going.
   </td>
   <td>Carrie Crowe, Felicity Brand
   </td>
   <td><a href="https://thegooddocs.slack.com/archives/C023G344YAJ">#content-strategy</a> Slack channel
   </td>
  </tr>
  <tr>
   <td>Website
   </td>
   <td>Project
   </td>
   <td>This is a project that will transition into maintenance mode.
   </td>
   <td>Alyssa Rock
   </td>
   <td><a href="https://thegooddocs.slack.com/archives/C019Y7ZCKR7">#website</a> Slack channel
   </td>
  </tr>
  <tr>
   <td>Knowledge base
   </td>
   <td>Project
   </td>
   <td>This is a project that will transition into maintenance mode.
   </td>
   <td>Ryan Macklin
   </td>
   <td>Content strategy meetings, general project meetings.
   </td>
  </tr>
  <tr>
   <td>Social media
   </td>
   <td>Activity
   </td>
   <td>Participating in <a href="https://docs.google.com/document/d/1H7fwgWD1t8IOutwRqItX31YhfgR6jKD1W3F_zkDXi0I/edit">social media</a> on behalf of TGDP is an activity that we recognize as important, but is not currently a high priority. We will continue to maintain a presence and be active when it suits us. 
   </td>
   <td>Carrie Crowe, Felicity Brand
   </td>
   <td><a href="https://thegooddocs.slack.com/archives/C023G344YAJ">#content-strategy</a> Slack channel
   </td>
  </tr>
  <tr>
   <td>Blog
   </td>
   <td>Activity
   </td>
   <td>Writing posts on our <a href="https://thegooddocsproject.dev/blog/">blog </a>is an activity that we recognize as important, but is not currently a high priority. We will continue to write and post when it suits us. 
   </td>
   <td>Ryan Macklin
   </td>
   <td><a href="https://thegooddocs.slack.com/archives/C01SDNR5X1C">#blog</a> Slack channel
   </td>
  </tr>
  <tr>
   <td>Project advocacy
   </td>
   <td>Activity
   </td>
   <td>Project advocacy is an activity that will become more important after our first release. Specifically it’s an activity we would conduct to meet particular goals like increase our user base, grow our community or expand our reach.
<p>
Currently our marketing is organic and informal, primarily through word of mouth and the Write the Docs community. In future we might look to more structured outreach in the form of podcast guesting, conference keynotes, meetups & events, and social media presence.
   </td>
   <td>Carrie Crowe, Felicity Brand
   </td>
   <td><a href="https://thegooddocs.slack.com/archives/C023G344YAJ">#content-strategy</a> Slack channel
   </td>
  </tr>
  <tr>
   <td>UX
   </td>
   <td>Service
   </td>
   <td>This is a service that spins up when required by different projects and working groups.
<p>
For now, UX remains as a focus area of the Content Strategy working group. This will help with management of tasks and workflow. In the future, this service may be promoted to be a peer of content strat, because it sits across the whole project. Something to aim for when we are more mature and have long-standing UX-ers in the project.
   </td>
   <td>TBD
   </td>
   <td> <a href="https://thegooddocs.slack.com/archives/C04PM4DTDST">#ux</a> Slack channel
   </td>
  </tr>
</table>
