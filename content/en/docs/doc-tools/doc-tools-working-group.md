---
title: "Working group"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "doc-tools"
weight: 004
toc: true
---

## Objective

One of the biggest hurdles to creating good documentation is setting up your initial documentation toolchain. Join this group to collaborate with our docs tool experts who are working on creating a Hugo/Docsy repository that can be easily cloned to set up a new documentation system for a project. 

The example site included in the project will use the templates from the Good Docs Project for each content type.

> Our primary goal is to provide a new documentation website starting point for anyone, almost instantly, with a single click of a button.

## Team

Point of contact: Bryan Klein

## Communication

Meetings:

Slack channel: 