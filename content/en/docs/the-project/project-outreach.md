---
title: "Project outreach"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "the-project"
weight: 020
toc: true
---

## Social media policy

Insert here.

## Representing The Good Docs Project

See [issue 67: Create outreach policy and plan](https://gitlab.com/tgdp/governance/-/issues/67)

## Project slide deck for events

See [issue 65: Create a project slide deck](https://gitlab.com/tgdp/governance/-/issues/65).
