---
title: "Release planning"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "releases"
weight: 008
toc: true
---

## Cadence

We typically have two releases a year. 

## Bridge names

We name our releases alphabetically after bridges across the world.

## Scope

Releases aren't just product-based. Each working group has to opportunity to commit to completing specific tasks per release. It may not always be appropriate. For example, the Blog.

## Tracking

GitLab issues, labels. Burndown chart. 

Role: release manager.