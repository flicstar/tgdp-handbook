---
title: "Project roadmap"
description: 
date: 
lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "releases"
weight: 004
toc: true
---

The project is in a growing and building phase.

2022 was spent growing the community and attracting contributors to our project. A lot of early building work happened, creating templates and putting structures in place to support the community.
 
The overall vision for 2023 is to focus on our products. 

## Product roadmaps

Our products include templates, the Chronologue and the Doc Tools registry. 

For more information about roadmaps, see the specific working groups for detail.

## Releases

### Dragon Bridge - Future

Planned release date: November 2023

[Dragon Bridge](https://en.wikipedia.org/wiki/Dragon_Bridge_(Da_Nang)) is a bridge over the River Hàn at Da Nang, Vietnam. 

![Dragon bridge](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/C%E1%BA%A7u_R%E1%BB%93ng.jpg/450px-C%E1%BA%A7u_R%E1%BB%93ng.jpg)


The Dragon release will see a focus on encouraging product adoption as well as user testing and validation. 

### Capilano Bridge - In progress

Due: June 2023

The Capilano release will see the first official version 1.0 release of our templates. 

### Brooklyn Bridge

Released: November 2022

[Details in GitLab](https://gitlab.com/groups/tgdp/-/milestones/2#tab-issues)

### Portland

Released: May 2022

[Details in GitLab](https://gitlab.com/tgdp/governance/-/wikis/Write-The-Docs-Portland-2022-Release).


