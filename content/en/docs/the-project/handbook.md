---
title: "Handbook"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "the-project"
weight: 034
toc: true
---

## About the handbook

A Hugo-based static site using the [Docsy](https://github.com/google/docsy/) theme. 

URL: TBD

Repo: TBD

## How to edit

Use the `Edit this page on GitLab` function to edit a page. This will automatically raise an issue on the repo {name}.

You can also manually log in issue in GitLab. 

Since the handbook is based on Hugo and docsy, you can follow the [procedure for editing the website](../website#how-to-edit) to edit the handbook.
