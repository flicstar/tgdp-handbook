---
title: "Logos and assets"
description: 
lead: 
date: 
lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "the-project"
weight: 016
toc: true
---

The [Brand Assets](https://gitlab.com/tgdp/brand-assets) repo holds images and assets for The Good Docs Project.

## Doctopus
The doctopus is our project mascot. Add information here about what the doctopus represents. How it can be shown holding different things and logos of other tech.

## How to use TGDP images

Are they free to use? Is there a license? 