---
title: "Tools and technology"
description: 
lead: 
date: 
lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "the-project"
weight: 012
toc: true
---


The [The Tech Team](../roles-and-responsibilities/#tech-team) are responsible for introducing, maintaining and retiring tools in our project. 

## Free tools

### Google

We have a Google account for the project, and we use the free tools that come bundled with Google, such as:

* Google meet
* Google drive for file storage
* Google forms for surveys.

### Slack

We use a free Slack workspace as our primary method of communication.

### Websites

We use SSG sites for the website, Chronologue and our handbook. We use a Hugo site with Docsy theme.

### GitLab

We started with GitHub and migrated to GitLab in 2022. We use GitLab for:

* file storage
* issue tracking
* work management (with Kanban boards)
* release planning

## Paid tools

We have the luxury of some community members lending their paid accounts to the project, such as:

* Zoom for meetings
* Miro for brainstorming
* blah for surveys.

## How to introduce a new tool to the project

Talk to the Tech Team about introducing new tools to the project. Post in the Slack channel, go to a meeting or raise an issue in the Technology Team repo. 