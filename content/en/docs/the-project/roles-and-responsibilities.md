---
title: "Roles and responsilities"
description: 
date: 2020-11-17T20:11:42+01:00
lastmod: 2020-11-17T20:11:42+01:00
draft: false
images: []
menu:
  docs:
    parent: "the-project"
weight: 008
toc: true
---

The Good Docs Project is powered by a great community and a team of dedicated folks who keep our project moving forward.
We are always looking for more people to help with:

- Running and participating in working groups.
- Onboarding and welcoming new community members.
- Assisting with the project's IT needs.

If you're interested in helping out, get in touch with someone from the team you're interested in.
The best way to contact these team members is on our Slack workspace, which you can get invited to by attending one of our [Welcome Wagon](/welcome) meetings.

## Project steering committee

The Project Steering Committee (PSC) is the official managing body for our project.

This team:

- Has a duty to define and uphold the project's mission, goals, and plans.
- Nominates and votes to approve individuals to key community roles and teams.
- Discusses and votes on project business and major project initiatives, such as requests-for-comment (RFCs).
- Is chaired by 1-3 individuals, who act as tie-breaking votes and who help ensure the efficiency and effectiveness of the PSC.

The current project steering committee members are:

- Aaron Peters, co-chair
- Carrie Crowe, co-chair
- Ryan Macklin, co-chair
- Alyssa Rock
- Bryan Klein
- Cameron Shorter
- Deanna Thompson
- Erin McKean
- Felicity Brand
- Gayathri Krishnaswamy
- Michael Park
- Tina Lüdtke

The committee is drawn from members of the community and is based on merit and interest.
The project steering committee:

- Aims to attract a diverse mix of personal characteristics, reflective of the community we represent.
- Avoids having employees of any one organization representing a controlling proportion of the PSC.
- May retire any time. If a PSC member becomes inactive for multiple quarters, it may be time to retire. (Ex-members will typically be welcomed back if they become active again.) An inactive member may be invited to retire. If unresponsive, they may be removed by the existing PSC.

If you are interested in serving on the PSC, let a current member of the PSC know about your interest.


## Community managers

This community managers ensure our community is healthy, vibrant, and safe for all who want to participate.

This team is responsible for:

- Fostering a positive community culture.
- Growing our community.
- Onboarding, mentoring, and training community members.
- Ensuring our community is safe and supported by:
  - Handling [Code of Conduct](/code-of-conduct) incidents.
  - Moderating our community forums.

The current community managers are:

- Alyssa Rock
- Carrie Crowe
- Deanna Thompson

If you are interested in serving as a community manager, let one of the community managers know.

If you experience an incident which makes you feel less safe in our community or less able to be your authentic self, contact the community manager with whom you have the most comfortable relationship and let them know.

The best way to contact these team members is on our Slack workspace in the **#ask-a-community-manager** channel or through a private direct message.
You can get invited to our Slack workspace by attending one of our [Welcome Wagon](/welcome) meetings.

For more information, see:

- [The Good Docs Project Code of Conduct](/code-of-conduct)


## Tech team

The Tech team helps with the project’s IT services and tooling needs, including repository access.

This team:

- Maintains access to all the project's IT services and tools.
- Enforces and maintains our security policies.
- Consults on IT related requests and needs.

The current Tech team members are:

- Alyssa Rock
- Bryan Klein
- Michael Park

If you are interested in joining the Tech team, let one of the members of this team know.

The best way to contact the team is on our Slack workspace in the **#tech-requests** channel.
You can get invited to our Slack workspace by attending one of our [Welcome Wagon](/welcome) meetings.


## Working groups

A working group consists of two or more templateers who are collaborating together to work on a Good Docs Project initiative or focus area of the project.
Working groups can be organized on a temporary, ad-hoc basis or can exist as a long-term, permanent basis as long as there is sufficient interest to maintain them.

The working group leads help manage our project's various working groups. The current working group leads for each working group are:

<table>
  <tbody>
    <tr>
      <th>Working group</th>
      <th>Lead(s)</th>
    </tr>
    <tr>
      <td>Templates - Alpaca (US/APAC)</td>
      <td>
        <ul>
          <li>Alyssa Rock</li>
          <li>Joanne Fung</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Templates - Macaw (APAC/EMEA)</td>
      <td>
        <ul>
          <li>Cameron Shorter</li>
          <li>Gayathri Krishnaswamy</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Templates - Dolphin (US/EMEA)</td>
      <td>
        <ul>
          <li>Deanna Thompson</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Chronologue</td>
      <td>
        <ul>
          <li>Valeria Hernandez</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Content strategy</td>
      <td>
        <ul>
          <li>Carrie Crowe</li>
          <li>Felicity Brand</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>Doc tools registry</td>
      <td>
        <ul>
          <li>Bryan Klein</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### Creating a working group
Anyone can create a working group for an initiative or focus area that you’re passionate about.
To start a group, you need at least two Good Docs Project community members who are interested in collaborating together.
(Otherwise, it’s also fine to work independently on an initiative.)

To create a working group:

- Create a Slack channel for your group, then in the list of members of your channel, you can right click each person to copy the link to use for them in the 'members' frontmatter list.
- Pick someone in your group to be the point of contact
- Decide on regular meeting cadence (weekly? bi-weekly? monthly?)
- Consider making a few general announcements to recruit other people toward your group on the #general Slack channel or on the main groups.io mailing list

### Responsibilities of active working groups
In order to be considered an active working group, your working group should strive to keep its responsibilities. Working group responsibilities include:

- Holding meetings and recording meeting notes
- The group should provide their group status, updated at least monthly. Adding notes to the Working Group page content is a great location for this information.
  Ideally this will be presented by one or more of its members to one of the two weekly Good Docs Project meetings.
  If attendance at the meeting is not possible, please provide your group status in the #general Slack channel or on the groups.io mailing list.
- All working group members should follow the Good Docs Code of Conduct when working together.
- The group members should try to keep any commitments that they make to the working group.
- Maintain at least two active contributing group members.
- Keep membership in the group open and allow new community members to join.

### Dissolving a working group

If your working group is no longer active or has achieved its objectives, you can dissolve the working group.
To dissolve the working group, give a 2 week notice to your group members and to the general Good Docs membership on the #general Slack channel and/or on the groups.io mailing list.

After two weeks, remove your group from the Handbook. Archiving the working group Slack channel is optional.


## Thank you to past team members

We are thankful for the hard work and dedication of these past team members.

Previous PSC members:

- Aidan Doherty
- Ankita Tripathi
- Becky Todd
- Clarence Cromwell
- Jared Morgan
- Jennifer Rondeau
- Jo Cook
- Morgan Craft
- Nelson Guya
- Viraji Ogodapola