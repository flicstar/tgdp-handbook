---
title: "Product and site"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "chronologue"
weight: 012
toc: true
---

## About the Chronologue site and docs


Repo: [gitlab.com/tgdp/chronologue/mock-tool](https://gitlab.com/tgdp/chronologue/mock-tool)

URL: TBD

## About the Chronologue docs site

Repo: [gitlab.com/tgdp/chronologue/docs](https://gitlab.com/tgdp/chronologue/docs)

URL: [docs-chronologue.netlify.app/](https://docs-chronologue.netlify.app/)

