---
title: "Writing process"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "chronologue"
weight: 008
toc: true
---


The writers write in Markdown and store their documentation on the [Chronologue Documentation](https://gitlab.com/tgdp/chronologue/docs) repository on Gitlab. 
We use HUGO as a static site generator for the documentation as we regularly update the docs and it is the project's standard tool.

The Chronologue working group starts its writing process when the template is in [Phase 6 - Improve the template with user feedback](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md#improve-the-template-with-user-feedback).

We decided to come in at this late stage because developing good templates is already quite time consuming and we don't want to add to the development cycle.

The following table describes what happens after a template has been released.

| Phase | Who does it? | What happens? |
| :---  |    :----:   |   :----  |
|1 - Plan | Working group lead| <li>Add the template to the documentation plan</li><li>Create an issue to track work <li>Assign content to a writer</li>|
|2 - Draft| Writer| <li>Create a new branch off of [`main`](https://gitlab.com/tgdp/chronologue/docs)<li>Create Chronologue documentation using the template</li><li>Keep a [friction log](https://developerrelations.com/developer-experience/an-introduction-to-friction-logging) of template/guide for later improvements</li>|
|3 - Review | Writer|<li>Create MR against the `main` branch </li><li>Assign WG lead or other writer as a reviewer</li><li>Improve content based on feedback</li>|
|4 - Commentary & Publication |Writer|<li>Add commentary to your content to highlight what makes it good</li><li>Merge the MR into the `main` branch to publish the content</li>|
|5 (if applicable) - Template improvements | Writer| <li>In the Templates repository, create a new branch off of [`dev`](https://gitlab.com/tgdp/templates)</li> <li>Make changes to the template based on your friction log notes.</li><li>Create a MR and add the template author as a reviewer</li>|