---
title: "Working Group"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "chronologue"
weight: 004
toc: true
---


## Objective

The primary task of the group is to create example documentation to increase the understanding of what good documentation can look like. The primary purpose is to educate people without a background in technical communication.

The Chronologue uses the Good Docs Project’s templates to create these examples, so the secondary purpose of the project is to act as a quality assurance for the Template working group.

The Chronologue, a fictional product, is a time-travel telescope that records astronomical events of the future and past. 

The Chronologue consists of:  

-   A time-travel telescope (fictional)
-   An API to transmit data between the telescope and the website
-   A website to view events recorded by the telescope

## Team and roles

This section describes roles within Chronologue and what they typically do. 
Members should take on one base role, and if bandwidth permits, they should take on a vertical role as well. 

### Base roles
- Working group lead (Lead) - Group representative that coordinates members, leads release planning, and ensures progress towards set goals.
- Individual contributor (IC) - Member that primarily writes code or documentation to make progress towards goals defined by the lead. 

### Vertical roles

> Some of the roles might be needed for a limited time, others might be needed on an ongoing basis

- Communication point with the larger project (ExtComms)- Attending the general US meeting to give updates on the Chronologue project.
- Internal documentation (IntComms) - Finding the gaps in internal docs in our current repos. Takes initiative to create internal docs, but doesn't have to do all on their own.
- Meeting management (MM) - Setting Chronologue meeting agendas and running the meetings.
- Project management (PM) - Coordinating GitLab issues, making sure that people get assigned, and tracking progress. Checking group members for status updates and making sure it's reflected in issues or other internal documentation.
- Onboarding -  Helping new Chronologue members choose their first task and get started.
Coaching and support (Coaching) - When Chronologue members get stuck or need assistance, giving them advice or support to become unblocked.
- Process - Defining how the group works in itself and with cross-functional stakeholders (primarily template group)

### Role summary

The table is organized as a [RACI chart](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example).
RACI is an acronym that stands for **R**esponsible, **A**ccountable, **C**onsulted, **I**nformed. 
Each letter in RACI represents a level of task responsibility on a project.



<table>
  <tr>
   <td>Role→ <br> Task↓</td><td>Lead</td><td>IC</td><td>ExtComms</td><td>IntComms</td><td>MM</td><td>PM</td><td>Onboarding</td><td>Coaching</td><td>Process</td></tr>
  <tr>
   <td>Running Chronologue meetings</td><td>A</td><td>C</td><td>C</td><td>I</td><td>R</td><td>I</td><td>I</td><td>I</td><td>C</td></tr><tr>
   <td>Onboarding new members
   </td><td>A</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>R</td><td>C</td><td>I</td></tr>
  <tr>
   <td>Release planning</td><td>A, R</td><td>R</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>C</td></tr>
  <tr>
   <td>Creating & maintaining the documentation plan</td><td>A</td><td>C</td><td>I</td><td>C</td><td>I</td><td>R</td><td>I</td><td>I</td><td>I</td></tr>
  <tr>
   <td>Assigning tasks within the group</td><td>A, R</td><td>C</td><td>I</td><td>I</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td></tr>
  <tr><td>Writing Chronologue documentation</td><td>A</td><td>R</td><td>I</td><td>I</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td>
  </tr><tr>
   <td>Web development</td><td>A</td><td>R</td><td>I</td><td>C</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td></tr>
<tr>
   <td>Maintaining docs tools</td><td>A</td><td>R</td><td>I</td><td>C</td><td>I</td><td>C</td><td>I</td><td>-</td><td>I</td>
  </tr>
  <tr>
   <td>Sharing Chronologue progress externally</td><td>C</td><td>C</td><td>A, R</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td>
  </tr>
  <tr><td>Creating internal documentation</td><td>R</td><td>R</td><td>I</td><td>A, R</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Coaching and supporting Chronologue members</td><td>C</td><td>C</td><td>I</td><td>I</td><td>C</td><td>C</td><td>C</td><td>A, R</td><td>I</td>
  </tr>
  <tr>
   <td>Reviewing MRs</td><td>C</td><td>R</td><td>I</td><td>I</td><td>I</td><td>A</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Merging MRs</td><td>R</td><td>R</td><td>I</td><td>I</td><td>I</td><td>A</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Writing release notes</td><td>A, R</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td><td>I</td><td>I</td><td>C</td>
  </tr>
  <tr>
   <td>Improve templates</td><td>C</td><td>A, R</td><td>C</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td>
  </tr>
</table>



## Consequences

### Positive impact

The proposal contributes to a better understanding of the Chronologue project’s responsibilities, ambitions, and ways of working. 
Furthermore, it establishes a firm foundation on which the working group can rely on when making future decisions. 
With this RFC, we aim to ensure a smoother process to create usable, understandable examples for people that want to use the Good Docs Project’s templates. 

### Possible negative impact

The mock tool website poses a possible risk to the maintainability of the fake tool. Since we want to divert from standard tooling (static generated site using HUGO), it can become a bottleneck if knowledgable members of the working group become unavailable. 
If we lose critical knowledge, we become less agile when it comes to resolving bugs or further development.  

### Mitigation strategy <a id="mitigation"></a>
To mitigate the risk of losing knowledgable members and becoming immobile, we want to supply comprehensive internal documentation about: 

* The framework we are working with and deviations from standard implementation (if applicable) 
* How the repository is organized
* How we approached styling (CSS) and how style files relate to the source code
* How to maintain vital parts of the website, including security updates of the framework and dependencies
* A reference document with links to more in-depth resources

Furthermore, we need to be very selective when it comes to implementing new feautres. 
The mock tool exists so we have something to write about when we test templates. 
However, if we need to document something that the mock tool can't really do, we need to come up with a solution, illustrated by the following example: 

> **Task at hand**: Test a template for a troubleshooting page. 

> **Problem**: The mock tool website is very simple. The user can only retrieve data, so there is not much to troubleshoot if you are only an end user. You can reload the page or search for a different thing, both of which are not per se tasks you need to document. 

> **Solution options**: As described in the [Function of the Chronologue](#function) section, the project consists of more than just the mock tool. We also have an API and the "physical" telescope itself to write about. So we can pick from the following solution options: 
>
>   * Pivot to a different part of the Chronologue: If we can't write troubleshooting information about the website, we may be able to write it in the context of using the API. If we can't write the docs about something tangible, we can still resort to our fictional telescope. For example, we can write a troubleshooting page for "Getting the Hadron Collider unstuck". 
>   * If the prior option is not feasible, let's say because we *need* a troubleshooting page for end users, then we would need to build a small feature that is really error prone. **However**, I would resort to this option only if really necessary. More features make the tool harder to maintain, and we want to build a stable product.


## Links and prior art

[Chronologue Figma Mockup](https://www.figma.com/proto/lvaAChlbueycET2ws9ZquS/Chronologue?node-id=902%3A1745&scaling=min-zoom&page-id=902%3A1640&starting-point-node-id=902%3A1745) created by Ulises de la Luz and Serena Jolley.

### Documentation page: Sidebars
![image.png](https://gitlab.com/tgdp/request-for-comment/-/raw/main/Accepted-RFCs/image.png)

