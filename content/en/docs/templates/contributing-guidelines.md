---
title: "Contributing guidelines"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "templates"
weight: 004
toc: true
---


## Time commitment

Most of our work is done by participating in one of our working groups, which generally meet weekly or bi-weekly for 1 hour a week. To effectively contribute templates to a project, all template writers are strongly encouraged to join one of the template writing working groups, especially if it is your first time contributing. Check our [community calendar](../community-calendar) or ask a member of our community for meeting times.

Working at a pace of 1-2 hours a week, most template projects take 4-9 months to complete. Keep in mind that we’ll take what you can give. You and your family come first, then work, then volunteering. So, if something in your life prevents you from working on your project, that's okay. Try to let your working group leader know if you won't be able to continue working for a space of time.

## Roles and resources

As you work on contributing templates to our project, various resources and members of our community are available to help you along the process. These include:

- **Templateers** - Any individual who contributes to the Good Docs Project (including you!).
- **The template working group leads** - These templateers oversee our overall template development process as a project manager and provide assistance to contributors working on templates. The working group leads also usually review and approve merge requests submitted to the templates repository.
- **Template mentors** - This group of experienced templateers lead template writing working groups where you can receive guidance and mentorship while working on your template.
- **Template peers** - Your fellow templateer peers are available to review templates during the research and community feedback phases. They include members of your template writing working group, but also members from the larger Good Docs Project community.
- **Template editors** - This group is in charge of making sure our templates follow standard conventions and model high quality writing. They maintain our style guide, make style guide decisions, notify the community about style guide changes, and help to ensure compliance with our style policies.

## Template issues and boards

All the templates that are actively being worked on or which could be worked on have a corresponding issue in the templates repository. Use the issue list or the kanban board to find a template to work on and track your template’s progress as it moves throughout the template writing phases.

- [Template issues list](https://gitlab.com/tgdp/templates/-/issues)
- [Templates in progress kanban board](https://gitlab.com/tgdp/templates/-/boards/4801048)


## Types of meetings

#### Co-working sessions

This is just a time to get together, put our heads down, and work on our projects while we're all together on the same call. It's nice because it gives you dedicated time to make sure you make some progress on your project. If you're also working in a paired writing model with a mentor, this is also a cha- nce to talk to the person you are pairing with. We often create breakout rooms so that paired writers can meet and talk to each other and sync up for a bit. That's nice because it makes it so that you don't have to schedule an additional meeting outside our normal meeting time.

#### Writer's workshop
For this meeting, we each take a turn talking about our project for about 5-10 minutes. Some people think it's just about giving a status update, but I try to encourage them to go deeper and try to come up with at least one discussion question they want to pose to the group to get some thoughts and ideas about their project. So, rather than thinking about it as a standup meeting (nothing against those, but I think they're boring and sometimes rote), it's better to think of it more like a poetry circle where we show each other our work and workshop it together. It's about bonding together as writers to improve at our craft.

#### Review sessions
On an ad-hoc/as-needed basis, we hold review sessions that mirror the co-working and writer's workshop sessions only these are focused on one template project specifically. When a member of our group has a template that has entered the community review phase, we schedule a review session where the writer shares their document with us and we spend the full meeting time silently making comments on the draft (often using the Conventional Comments method, but not required). Then we'll usually schedule a review workshop a few weeks later after the writer has had a chance to process all the commentary and they'll lead us in a discussion about comments that they wanted to follow up on.

We normally alternate between co-working sessions and writer's workshops every other week. The review sessions are scheduled on an as-needed basis.