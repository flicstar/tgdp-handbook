---
title: "Required deliverables"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "templates"
weight: 008
toc: true
---


See [Template Deliverables](https://gitlab.com/tgdp/templates/-/blob/main/template-deliverables.md) for more detailed information about each template deliverable.

Currently, for a template set to be considered complete, each template set should have these documentation deliverables:

<table>
  <tr>
    <th>Deliverable</th>
    <th>Description and purpose</th>
    <th>Required?</th>
  </tr>
  <tr>
    <td>Template file</td>
    <td>The template file is the raw template for the type of document you are creating. It provides a rough outline of the suggested content and a few embedded writing tips for how to fill in the different sections of the template.</td>
    <td>Required</td>
  </tr>
  <tr>
    <td>Template guide</td>
    <td>This guide provides a deeper explanation of how to fill in the template. It provides a lightweight introduction to the purpose of this documentation and explains how to fill in each section of the document. Any information that is essential to filling out the template should be noted in this guide.</td>
    <td>Required</td>
  </tr>
  <tr>
    <td>Deep dive</td>
    <td>This optional guide is a supplementary guide that can provide additional helpful information to template users such as key research, deeper theories, brainstorming, or pre-writing steps that are too comprehensive or lengthy to be included in the template guide. What is included in this guide may be unique to each template.</td>
    <td>Optional</td>
  </tr>
  <tr>
    <td>Template example</td>
    <td>After a template project is complete, our Chronologue working group will create an example of the template. While creating the example, the Chronologue group will test whether your template is user-friendly and can be used in a real documentation project. If you're still involved in the community during this phase, these team members might reach out to you for feedback or to collaborate on possible template revisions.</td>
    <td>Required, but occurs after the template writing process</td>
  </tr>
</table>
