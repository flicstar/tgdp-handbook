---
title: "The Good Docs Project Handbook"
description: 
date: 2020-11-17T20:11:42+01:00
lastmod: 2020-11-17T20:11:42+01:00
draft: false
images: []
menu:
  docs:
    parent: "docs"
weight: 004
toc: true
---

## Get involved

Find out how to join our community, and what you can do while you're here. 

## Products

Learn more about the things we make in our project.

* Templates
* Chronologue
* Dev Tools Registry

## Style guide

Check the guidelines for how to write and talk about our products.

## The project

Meet the team, see how we work, and find out more about how we run this open source project. 