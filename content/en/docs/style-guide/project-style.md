---
title: "Project style guide"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "style-guide"
weight: 004
toc: true
---

This style guide is for contributors to The Good Docs Project. It provides general guidance to anyone who contributes to the project's documentation, which includes templates, the Chronologue, knowledge base content, and our website. 

## Our preferred style guide

We use the [Google developer documentation style guide](https://developers.google.com/style) as our parent style guide. For a quick summary, see the [Google style guide highlights](https://developers.google.com/style/highlights). The rest of this document describes our project-specific customizations.

Our project uses standard American spelling, and our preferred dictionary is the [American Heritage Dictionary](https://ahdictionary.com/).

When writing documentation for our project, align with the Google developer documentation style guide’s [voice and tone](https://developers.google.com/style/tone).


## Glossary of preferred terms


### Our project name


<table>
  <tr>
   <td><strong>Preferred Term</strong>
   </td>
   <td><strong>Avoid These Terms</strong>
   </td>
   <td><strong>Explanation</strong>
   </td>
  </tr>
  <tr>
   <td>The Good Docs Project
   </td>
   <td>Good Docs
<p>
the Good Docs project
   </td>
   <td>The words “the” and “project” are part of this project’s name. The project name is always represented using title case.
   </td>
  </tr>
  <tr>
   <td>TGDP
   </td>
   <td>
   </td>
   <td>Abbreviating is okay internally and in our knowledge base, but never for external or public-facing documents or on our website. 
   </td>
  </tr>
</table>



### Other terms

The table provides guidelines about the terms you should and should not use for consistency, listed in alphabetical order:


<table>
  <tr>
   <td><strong>Preferred Term</strong>
   </td>
   <td><strong>Avoid These Terms</strong>
   </td>
   <td><strong>Explanation</strong>
   </td>
  </tr>
  <tr>
   <td>for example
   </td>
   <td>e.g.
   </td>
   <td>Avoid non-English words.
   </td>
  </tr>
  <tr>
   <td>people with disabilities
   </td>
   <td>
<ul>

<li>the disabled

<li>disabled people

<li>people with handicaps

<li>the handicapped
</li>
</ul>
   </td>
   <td>Use inclusive and bias-free language. For more information, see [Accessible Writing](#accessible-writing).
   </td>
  </tr>
  <tr>
   <td>that is
   </td>
   <td>i.e.
   </td>
   <td>Avoid non-English words.
   </td>
  </tr>
</table>

## Accessible writing

Documentation should be written in a way that supports people with disabilities and users with various input methods and devices. Improving accessibility also helps make documentation clearer and more useful for everyone.

For resources on making your writing more accessible, see the following:

* [Writing accessible documentation - Google developer documentation style guide](https://developers.google.com/style/accessibility)
* [Accessibility guidelines and requirements - Microsoft Writing Style Guide](https://docs.microsoft.com/en-us/style-guide/accessibility/accessibility-guidelines-requirements)
* [Writing for Accessibility - Mailchimp Content Style Guide](https://styleguide.mailchimp.com/writing-for-accessibility/)


## Inclusive and bias-free writing

When contributing to this project, you should strive to write documentation with inclusivity and diversity in mind. Inclusive language recognizes diversity and strives to communicate respectfully to all people. This kind of language is sensitive to differences and seeks to promote equal opportunities. 

For resources on making your writing more inclusive, see the following:


* [Inclusive documentation - Google developer documentation style guide](https://developers.google.com/style/inclusive-documentation)
* [The Conscious Style Guide - a collection of resources](https://consciousstyleguide.com/)
* [Bias-free communication - Microsoft Writing Style Guide](https://docs.microsoft.com/en-us/style-guide/bias-free-communication)
* [Guidelines for Inclusive Language - Linguistic Society of America](https://www.linguisticsociety.org/resource/guidelines-inclusive-language)


## How the style guide is updated

{Indicate here how frequently your style guide is reviewed, who owns the style guide, and how contributors can provide feedback on your style guide.}


## Revision history

The following table describes the history of all decisions and revisions made to this style guide over time. This guide uses the Major.Minor.Patch [semantic versioning](https://semver.org/) convention.


<table>
  <tr>
   <td><strong>Edition</strong>
   </td>
   <td><strong>Date</strong>
   </td>
   <td><strong>Lead Author(s)</strong>
   </td>
   <td><strong>Link to Repository Commit/Tag</strong>
   </td>
  </tr>
  <tr>
   <td>{0.1}
   </td>
   <td>{YYYY-MM-DD}
   </td>
   <td>{Your name here}
   </td>
   <td>First draft of Style Guide
   </td>
  </tr>
</table>
