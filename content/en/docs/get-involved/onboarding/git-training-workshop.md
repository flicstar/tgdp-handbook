---
title: "Git training workshop"
description: "Git training workshop"
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "onboarding"
weight: 008
toc: true
---
## Welcome!

If you’ve received a link to this page, it’s because you’ve expressed interest in attending one of the upcoming Git Training sessions offered by the Good Docs Project.
This page explains what the training includes to let you know what you can expect.


## What is included in this training?

The Good Docs Project Git Training is a 3-hour workshop that teaches participants the basics of using Git and related tools needed to contribute to the Good Docs Project:

* Learn general concepts about Git, such as the purpose of version control systems, how Git is used in docs-as-code systems and some of the key terms used in Git.
* Learn about the basics of working in a CLI (command line) environment.
* Learn about the basics of working in Markdown.
* Learn how to use Microsoft Visual Studio Code (a free code editor) to contribute to a Git repository.
* Set up their SSH keys to contribute to the Good Docs Project.
* Fork and clone a GitHub repository and make their first contribution.
* Make their first contribution from beginning to end, including working on a specific issue and submitting a pull request.

Before the workshop, participants will need to:

* Watch a video presentation about Git.
* Read an article about working in a CLI environment.
* Download required software.
* Set up necessary accounts and permissions.

At the end of the workshop, participants will have a final homework assignment to create a personal knowledge base system in GitLab to help consolidate their learning and ensure they have successfully completed the course.


## Who is eligible to take this workshop?

Each workshop can have up to 6 registered participants at a time.
The workshop is offered free of charge.

This workshop is intended for new or seasoned contributors to the Good Docs Project.
In order to take this training, a participant must:

* Be an active member of the Good Docs Project community who has participated in the project for at least a month. Participation can include regularly attending working group meetings or making meaningful contributions to various Good Docs Project initiatives.
* Be recommended for this training by a working group leader or other member of good standing in the Good Docs Project community.
* Demonstrates a desire to contribute meaningfully to the Good Docs Project over the next few months or more.

Additionally, we can offer this training to members of the community who want to eventually become Good Docs Project Git trainers themselves. However, priority registration will be given to new members of the community.


## When is the training offered?

The Good Docs Project offers this training on an as-needed basis when there are 2 or more new community members who could benefit from Git training.
When 2 or more community members are interested in the training and meet the eligibility requirements, we will schedule an upcoming training schedule.
The times and dates will be determined based on the schedule, time zones, and availability of the trainers and the participants.


## How can I register for the training?

Contact either one of the Good Docs Project community managers on Slack.

If you are not a member of the Good Docs Project but would like to become one, attend one of our [Welcome Wagon meetings](welcome.md).
