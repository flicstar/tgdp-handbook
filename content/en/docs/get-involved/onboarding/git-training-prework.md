---
title: "Git training pre-work"
description: "Git training pre-work"
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "onboarding"
weight: 004
toc: true
---
## Get ready!

The Good Docs Project is excited that you are enrolled in our upcoming Git Training Workshop.
See [Git training workshop](git-training) for more information about the workshop itself.

Before the day of the workshop, we need you to complete the following sets of tasks:


### Check that you have access to the training

*Estimated time to complete: 10-30 minutes*

- **Check that you have been invited to the Slack channel for our upcoming training on Slack.** By now, you should have been added to this channel. This channel is a place where you can communicate with the trainers and ask questions as needed.
- **Fill out the Git Training Registration form.** This link was provided to you in the training Slack channel. We need you to fill out this form to help us ensure everything is set up correctly when you arrive.
- **Check for a meeting invitation to the training.** By now, you should have received a Google calendar invitation through your email.  This invitation includes the Zoom link to attend the training in the Location field.


### Install the necessary applications and create accounts

*Estimated time to complete: 1 hour*

- **Download and install [Microsoft Visual Studio Code (VS Code)](https://code.visualstudio.com/download) for your operating system.** VS Code is free and it is the code editor we want you to work in during the Git Training Workshop. It works on Mac, Windows, and Linux.
- **Download and install [Git](https://git-scm.com/downloads) on your operating system.** Git is also free.
- **Follow the instructions in this video: [Integrate Git Bash with Microsoft Visual Studio Code](https://youtu.be/mvOWrU4leOM).**
- **Create a [GitLab account](https://gitlab.com/users/sign_up).** GitLab accounts are free.


### Complete the pre-training work

*Estimated time to complete: 2-3 hours*

We ask you to do a little bit of pre-work to ensure you understand some important foundational concepts before you attend the Git Training Workshop.
Completing these exercises before class ensures that we can spend more of our time giving you hands-on help with Git during the workshop.

- **Watch this video: [Git for the True Beginner](https://youtu.be/vi_TDlW5Bf0).** This 28 minute video will introduce you to important concepts and terminology about Git before you attend the workshop.
- **Complete this tutorial: [Introduction to the command-line interface](https://tutorial.djangogirls.org/en/intro_to_command_line/).** This tutorial will explain you to some of the basics about working in a command-line interface (CLI), which is important when working in Git.
- **Complete this tutorial: [Markdown tutorial](https://www.markdowntutorial.com/).**  This tutorial will give you some hands-on practice with how to format text in Markdown, which is a common text format we use at the Good Docs Project in our templates and our website. Markdown can be easily converted into HTML by static site generators.

## Need help or have a question?
Ask for help at any time in the training Slack channel on the Good Docs Project Slack workspace.
