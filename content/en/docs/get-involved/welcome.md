---
title: "Welcome"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "get-involved"
weight: 004
toc: true
---

Welcome to the Good Docs Project! If you’re reading this guide, that probably means you’d like to get involved and start contributing to the project. This document should hopefully help you become a full templateer (which is how we refer to the members of our community).

One of the core missions of the Good Docs Project is to provide high-quality documentation templates to open source software projects and beyond. However, we also engage in many other similar initiatives around docs advocacy, docs education, and docs tooling. We value all contributions to the Good Docs Project initiatives, including templates and our other initiatives. The [Join the community](#join-the-community) section of this guide explains how to get involved in those other related initiatives and provides links for more information.

## Before you start

Before starting, ensure you are familiar with the Good Docs Project’s goals, key concepts, and workflows. It might also possibly help to learn more about our project's personas. For more information, see [About the Good Docs Project](../../the-project/about/).

We expect all members of our project to be nice to each other and to follow our [Code of Conduct](../code-of-conduct/) when interacting with other members of the Good Docs Project.

## Join the community

To become a full-fledged templateer, you’ll want to join our communication channels so that you can talk to us:

- **Slack** - Our [Slack workspace](https://thegooddocs.slack.com/) is one of the primary means of communicating with members of our project. After joining our workspace, join the `#welcome` channel to introduce yourself to the welcoming committee. Consider also joining these Slack channels if you plan to work on a template:
  - `#templates`
  - `#welcome`
  - One of the community-docs channels (based on your preferred time zone and meeting times)
- **Working groups** - Our project is organized into several different working groups that meet on a regular basis to work on the project’s key initiatives. One of the best ways to get started with our project is to join and meet with one of our working groups. See [The Good Docs Project Working Groups](../the-project/roles-and-responsibilities/#working-groups) for a list of our current active groups. NOTE: If you plan to contribute to our project by writing templates, you will be required to join one of the template working groups.
- **Weekly meetings** - The project leaders hold weekly meetings to discuss project-level decisions. Feel free to join one of these meetings to introduce yourself to the project leaders and discover next steps for getting involved in the project. See the [community calendar](../community-calendar) for meeting times.

As you begin to join our project, remember that this is a project composed entirely of volunteers. We love to welcome new members, but want to be careful not to burn out our core project contributors. This level of mindfulness helps us ensure that we retain our project’s capacity to produce high-quality work. As such, we ask that you respect the time of our project maintainers and contributors.
(And expect us to respect your time in return!)
