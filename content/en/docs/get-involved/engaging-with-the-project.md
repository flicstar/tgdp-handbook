---
title: "Engaging with the project"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "get-involved"
weight: 020
toc: true
---

## Activities
While you’re here, the community has activities you can get involved with beyond contributing to a working group, such as our book club and our job seeker support group.

A lot of other work happens around the project as well. 

* The [style guide]() group discusses style issues and makes decisions about language usage and tone for our templates.
* You might feel inspired to write a post for our project [blog]().
* Updating content in this [Handbook](../../the-project/handbook).

### Book club

About the book club.

### Job seeker support

About job seeker support. Two slack channels.


## Showing your work

Your experience with TGDP is real-world technical writing experience and you are encouraged to talk about your work publicly, share it on your LinkedIn profile, and include it in your portfolio.

* [Listing your work with The Good Docs Project on LinkedIn](https://docs.google.com/document/d/1gyur31IhPeLmsZhvpo2CNvgmZhKhdpaPnbP9H26kItU/edit#heading=h.s3wot7e9xqnk)
* [Share and publicize your work with TGDP](https://gitlab.com/tgdp/governance/-/issues/87)

