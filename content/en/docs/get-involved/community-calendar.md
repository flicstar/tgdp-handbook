---
title: "Community calendar"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "get-involved"
weight: 016
toc: true
---


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.7/jstz.js"></script>

<iframe id="calendar_container" src="https://calendar.google.com/calendar/embed?src=gooddocsproject%40gmail.com&ctz=Australia%2FSydney" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

<script type="text/javascript">
  var timezone = jstz.determine();
  var pref = 'https://www.google.com/calendar/embed?src=gooddocsproject%40gmail.com&ctz=';
  var iframe_src = pref + encodeURIComponent(timezone.name().replace(' ' ,''));
  document.getElementById('calendar_container').src = iframe_src;
</script>

## Join a meeting

If you'd like to attend regularly, you should ask the meeting lead to add you to the meeting invite. This means you can:
* Log in without approval from the meeting lead.
* Have the invite added to your personal calendar.

## Subscribe to the calendar

You can view Good Docs Project meetings and events on your personal calendar by subscribing to the Good Docs Project public calendar.
To subscribe:

1. Click **+** next to the **Google Calendar** link in the lower right corner of the Good Docs public calendar.
2. When your personal Google calendar loads, you'll see the **Add calendar** dialog box. Click **Add**.

## Add an event to the community calendar

If this is your first time adding an event to the Google calendar, check with the Tech Team to see if you've been added to the project's list of contacts. Once you are in the project's Contact List, all meeting invites from you will automatically show up on the community calendar.

1. Set up the meeting invite in your Google calendar.
2. In the meeting invite, under Guest Permissions, turn off the See guest list option. (This step is required to maintain community member privacy.)
3. Invite The Good Docs Project account: gooddocsproject@gmail.com

If you are listed on the project's Contact List, all meeting invites from you will automatically show up on the community calendar.

After the invite is sent to the project account, the Tech Team will accept the invite on the project's behalf within a few business days.