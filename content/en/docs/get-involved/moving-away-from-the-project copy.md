---
title: "Moving away from the project"
description: 
date: 
Lastmod: 
draft: false
images: []
menu:
  docs:
    parent: "get-involved"
weight: 024
toc: true
---


Work and family comes first for all of us, and we know that burnout is real. We have scheduled downtime over the Christmas and New Year period; and following conferences and events where we have been particularly active (usually following a Write the Docs conference). 

Do you just need to take a short break? Often, project members step away from the project for weeks or months at a time, and then come back when they can be fully present. Other times, you know you’ve come to the end of the road with the project. 

{{< alert icon="♥" text="We strive to foster a positive and inclusive community. If you’ve experienced something you’d like to report, you can raise an issue with the Community Managers." />}} 


### Communicating your absence

No matter whether you’re stepping away for a short time, or leaving forever, communication is the key. We do appreciate being kept in the loop - there is no judgment and no obligation. Let your working group know, handover any open work and accept our best wishes to you.

Here are some example Slack messages people have used in the past:

> FYI y’all, I’m taking a short break from the project to focus on things. 
> I’ll be back in mid/late April. See you in a short bit. :smiling_face:

We have a policy to remove inactive Slack members and remove access to our GitLab repos.
